const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];


const createDiv = document.createElement('div')
const createList = document.createElement('ul')

createDiv.setAttribute('id', 'root')
createDiv.appendChild(createList)

books.forEach((book) => {
    try {
        if (!book.author) {
            throw new Error("В об'єкті відсутня властивість 'author'");
        }
        if (!book.name) {
            throw new Error("В об'єкті відсутня властивість 'name'");
        }
        if (!book.price) {
            throw new Error("В об'єкті відсутня властивість 'price'");
        }

        const createLi = document.createElement('li')
        createList.appendChild(createLi)
        createLi.textContent = `${book.name} --- ${book.author}, Ціна: ${book.price}`
    } catch (error) {
        console.error(`Помилка: ${error.message}`);
    }
})

console.log(createDiv);








  